package sample;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

public class LoginController {

    @FXML
    private TextField usernameTextfield;
    @FXML
    private PasswordField passwordField;

    private SampleAuthorization auth;

    public LoginController() {
        auth = new SampleAuthorization();
    }

    @FXML
    public void handleLogin() {
        if(auth.isAdminAuthorized(usernameTextfield.getText(), passwordField.getText())) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Authorized");
            alert.setContentText("Successfully authorized");
            alert.setHeaderText("Welcome");
            alert.show();
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Not Authorized");
            alert.setContentText("Authorization Failure");
            alert.setHeaderText("Something went wrong");
            alert.show();
        }
    }
}
